/***********************************************************************
	@Author: Jeremy Fardilha
	@name: DF03_Account
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Data factory for account
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
@isTest
public with sharing class DF03_Account {

    /*******************************************************************************
        *LBC Project                  	                - Créer des accounts selon un nombre désiré
		@name   createAccounts
        @param  Integer	numberOfAccounts                - Nombre d'account à créer
        @param  Boolean	doInsert                        - Indique si on les insert ou pas
        @return List<Account>	      			        - List d'account générée   
	*****************************************************************************/
       //Account creation
    public static List<Account> createAccounts(Integer numberOfAccounts, Boolean doInsert) {
        List<Account> accounts = new List<Account>();
        for ( Integer i = 0 ; i < numberOfAccounts ; i++ ) {
            
            Account account = new Account( 
                Name = 'Test Account' + Math.random(), 
                CompanySiretNumber__c = '12345678901112',
                BillingStreet = i + 'Paris street',
                BillingCity = 'Paris',
                BillingCountry = 'France',
                BillingPostalcode = '75001',
                Segmentation__c = 'SSII',
                CompanyIndustry__c = 'Beauty',
                BillingState = 'Île-de-France'
                );  
            accounts.add(account);
        }

        if(doInsert) {
            insert accounts;
        }

        return accounts;

    }
}