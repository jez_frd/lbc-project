/***********************************************************************
	@Author: Jeremy Fardilha
	@name: SM01_Subscription
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Service Manager for Subscription
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class SM01_Subscription {

	public static final String IMMO_NEUF 	 	 	 	= AP01_ConstantUtility.IMMO_NEUF;
	public static final String CONFORME  	 	  		= AP01_ConstantUtility.CONFORME;
	public static final String PARENT_BUNDLE 	 		= AP01_ConstantUtility.PARENT_BUNDLE;
	public static final String ELEMENTS_MANQUANTS 		= AP01_ConstantUtility.ELEMENTS_MANQUANTS;
	public static final String PUB_LOCAL 		  		= AP01_ConstantUtility.PUB_LOCAL;
	public static final String EFFECTIVE_ENDATE_FIELD   = AP01_ConstantUtility.EFFECTIVE_ENDATE_FIELD;

	/*******************************************************************************
        *LBC Project                  	                    - Verifie si une subscription est conforme avec ses parents
		@name   checkConformity
        @param  List<SBQQ_Subscription__c>	subsScope       - Souscription non conforme
        @return Map<String, List<SBQQ_Subscription__c>>	    - Renvoie une liste de souscription conforme       
	*****************************************************************************/
	public static Map<String, List<SBQQ_Subscription__c>> checkConformity(List<SBQQ_Subscription__c> subsScope) {
		Map<String, List<SBQQ_Subscription__c>> subsMap = new Map<String, List<SBQQ_Subscription__c>>();
		List<SBQQ_Subscription__c> subsConformedMap = new List<SBQQ_Subscription__c>();
		List<SBQQ_Subscription__c> subsConformedWithParent = new List<SBQQ_Subscription__c>();
		SBQQ_Subscription__c subChecked = new SBQQ_Subscription__c();
		//Check de la conformité des subscription
		for(SBQQ_Subscription__c sub : subsScope) {
		   sub.MissingInformations__c = '';
		   if (sub.ComplianceProcess__c == IMMO_NEUF) {
			   // Check de la conformité de la subscription
				subChecked = setConformiteImmoNeuf(sub);

			} else if (sub.ComplianceProcess__c == PUB_LOCAL) {
				if(sub.isEffectiveDate__c) {
					subChecked = setConformitePub(sub);
				}
			}

			//Vérification de la conformité parrent
			if (subChecked.Souscription_Conforme__c) {
				//Ajout dans une list pour update après si conforme
				subsConformedMap.add(subChecked);

				if (subChecked.SBQQ_RequiredById__c != null) {
					//Check en mémoire les subs parents à mettre à jour
					subsConformedWithParent.add(subChecked);
				   //setParentConforme(sub.SBQQ_RequiredById__c);
			   }
			} 
		}

		//map contenant toutes les subs conformes
		subsMap.put(CONFORME,subsConformedMap);
		//map contenant toutes les subs avec parents à mettre à jour
		subsMap.put(PARENT_BUNDLE, subsConformedWithParent);

		return subsMap;
	}

	/*******************************************************************************
        *LBC Project                  	                    - Verifie si une subscription est conforme
		@name   setConformiteImmoNeuf
        @param  SBQQ_Subscription__c	sub       			- Subscription à vérifier
        @return SBQQ_Subscription__c	      				- Subscription vérifié      
	*****************************************************************************/
    public static SBQQ_Subscription__c setConformiteImmoNeuf(SBQQ_Subscription__c sub) {
		//Les champs sont stockés dans un MDT, qui permettra de les modifier rapidement sans MEP
		List<String> fieldsMandatory = PR03_CustomMetadata.getImmoNeufMdt();

		//On verifie que chaque champ n'est pas vide
		//Si vide on stock dans missing information qui permet de savoir si la subs est conforme ou pas
		for(String field : fieldsMandatory){
			if(String.isBlank((String)sub.get(field))) {
				sub.MissingInformations__c += field + ' ';
			}
		}

		if(String.isBlank(sub.MissingInformations__c)) 
		{
			sub.Souscription_Conforme__c = true;
        } else {
			sub.Souscription_Conforme__c = false;
		}
        return sub;
    }
	
	/*******************************************************************************
        *LBC Project                  	                    		 - Verifie si un parent doit être mis en conformité pour les type immo
		@name   setParentConforme
		@param  List<SBQQ_Subscription__c> 	subsConformedMap     	 - List de subscription conforme avec parent
        @return ist<SBQQ_Subscription__c>	      				     - List de subscription parents     
	*****************************************************************************/
	public static List<SBQQ_Subscription__c> setParentConforme(List<SBQQ_Subscription__c> subsConformedMap ) {
		
		Set<Id> idChilds = new Set<Id>();
		for(SBQQ_Subscription__c sub : subsConformedMap ) {
			idChilds.add(sub.id);
		}
		//Récupération des parents
		List<SBQQ_Subscription__c> subsWithParent =  PR01_Subscription.getSubscriptionByIdAndStatut(ELEMENTS_MANQUANTS, false, idChilds);
		//Mise à jour de la conformité
		for(SBQQ_Subscription__c sub : subsWithParent ) {
			sub.Souscription_Conforme__c = true;
		}

		return subsWithParent;
	}

	/*******************************************************************************
        *LBC Project                  	                    - Verifie si une subscription est pour les types pub
		@name   setConformitePub
        @param  SBQQ_Subscription__c	sub       			- Subscription à vérifier
        @return SBQQ_Subscription__c	      				- Subscription vérifiée      
	*****************************************************************************/
    public static SBQQ_Subscription__c setConformitePub(SBQQ_Subscription__c sub) {
		if(sub.isEffectiveDate__c){
			sub.Souscription_Conforme__c = true;
		} else {
			sub.Souscription_Conforme__c = false;
			sub.MissingInformations__c += EFFECTIVE_ENDATE_FIELD + ' ';
		}

		return sub;
	}

}
