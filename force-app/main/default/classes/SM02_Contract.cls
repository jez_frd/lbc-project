/***********************************************************************
	@Author: Jeremy Fardilha
	@name: SM02_Contract
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Service manager contract
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class SM02_Contract {

	/*******************************************************************************
        *LBC Project                  	              - Met à jour tous les contrats liés à à une subs
		@name   updateContractEndDateFromSubs
        @param  list<SBQQ_Subscription__c>	subs      - Subscription mis à jour
        @return List<Contract>	      				  - Contrat à mettre àjour    
	*****************************************************************************/
    public static List<Contract> updateContractEndDateFromSubs(list<SBQQ_Subscription__c> subs) {
		Boolean isTerminate;
		Date terminatedDate;
		Date endDate;
		Set<Id> idCons = new Set<Id>();

		//On stock les id des contrats pour les requêtes
		for (SBQQ_Subscription__c sub :subs) {
			if(!String.isEmpty(sub.SBQQ_Contract__c)) {
				idCons.add(sub.SBQQ_Contract__c);
			}
		}

		try {
			//On récupère les contrats lié au subs
			List<Contract> conts = PR02_Contract.getContractWithSubs(idCons);
			for (Contract con : conts) {
		
				isTerminate = true;
				terminatedDate = con.EndDate;
				endDate = con.EndDate;
				
				// algo pour la mise à jour de la date
				for (SBQQ_Subscription__c sub : con.SBQQ_Subscriptions__r) {
					if (sub.SBQQ_TerminatedDate__c == null) {
						isTerminate = false;
					} else if (sub.SBQQ_TerminatedDate__c != null && terminatedDate < sub.SBQQ_TerminatedDate__c) {
						terminatedDate = sub.SBQQ_TerminatedDate__c;
					}
					if (sub.SBQQ_EndDate__c != null && endDate < sub.SBQQ_EndDate__c) {
						endDate = sub.SBQQ_EndDate__c;
					}
				}
				
				if (isTerminate) {
					con.EndDate = terminatedDate;
				} else {
					con.EndDate = endDate;
				}
												
			}
			
			//maj du contrat
			SObjectAccessDecision decision = Security.stripInaccessible(
				AccessType.UPDATABLE,
				conts);
			update decision.getRecords();  

			Logs.info('ContractEndDateAdapterTrigger', 'SBQQ_Subscription__c Trigger', ' Update' + conts.size() + ' contrats');

			return conts;

		} catch(Exception e) {
				Logs.error('ContractEndDateAdapterTrigger','Error SBQQ_Subscription__c Trigger processing', e);
		}

		return null;
	}
}
