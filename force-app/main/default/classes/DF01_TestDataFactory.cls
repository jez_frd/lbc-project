/***********************************************************************
	@Author: Jeremy Fardilha
	@name: DF01_TestDataFactory
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Data Factory main class
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class DF01_TestDataFactory {

    private static final Set<String> COMPLIANCE     = AP01_ConstantUtility.COMPLIANCE;  

	/*******************************************************************************
        *LBC Project                  	                - Test data factory pour toute les classes
		@name   testDataFactory
	*****************************************************************************/
	public static void testDataFactory() {
        List<Account> accs = DF03_Account.createAccounts(1, true);
        List<Acces_beneficiaire__c> accBens = DF02_AccesBeneficiaire.createBeneficiaireWithAcc(accs, true);
        List<SBQQ_Subscription__c> subsParent = DF04_Subscription.createSubs(COMPLIANCE, accs.get(0).ID, accBens.get(0).ID,  null, true);
		List<SBQQ_Subscription__c> subsChild = DF04_Subscription.createSubs(COMPLIANCE, accs.get(0).ID, accBens.get(0).ID, subsParent.get(0), true);
		List<Contract> contract = DF05_Contract.createContracts(subsParent, true);
	}

}
