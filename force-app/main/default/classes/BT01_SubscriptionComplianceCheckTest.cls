/***********************************************************************
	@Author: Jeremy Fardilha
	@name: BT01_SubscriptionComplianceCheckTest
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Test class of SubscriptionComplianceCheck
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
@isTest
private class BT01_SubscriptionComplianceCheckTest {

    /*******************************************************************************
        *LBC Project                  	                - Test du batch BT01
		@name   testConformite
	*****************************************************************************/
    @isTest
    private static void testConformite() {

        //Lancement du TDF
        DF01_TestDataFactory.testDataFactory();
        
		Test.startTest();
		    Database.executeBatch(new BT01_SubscriptionComplianceCheck(), 200);
		Test.stopTest();

        System.assertEquals(3, [SELECT Id FROM SBQQ_Subscription__c WHERE Souscription_Conforme__c = true].size(), 'Nombre de subs conforme');
        System.assertEquals(9, [SELECT Id FROM SBQQ_Subscription__c WHERE Souscription_Conforme__c = false].size(), 'Nombre de subs non conforme');
        System.assertEquals(14, [SELECT Id FROM SBQQ_Subscription__c].size(), 'Total des subs');
    }
}