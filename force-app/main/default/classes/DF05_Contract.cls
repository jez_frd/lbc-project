/***********************************************************************
	@Author: Jeremy Fardilha
	@name: DF05_Contract
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Data factory for contract
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class DF05_Contract {
    
    /*******************************************************************************
        *LBC Project                  	                - Créer des contrats
		@name   createContracts
        @param  List<SBQQ_Subscription__c>	subs        - Liste des subs à lier
        @param  Boolean	doInsert                        - Indique si on les insert ou pas
        @return List<Contract>	                        - List contrat généré  
	*****************************************************************************/
    public static List<Contract> createContracts(List<SBQQ_Subscription__c> subs,  Boolean doInsert) {
        List<Contract> conts = new List<Contract>();
        boolean jdd;
        for(SBQQ_Subscription__c sub : subs ) {
            Contract con = new Contract(
                Name = 'Contrat ' + Math.random(),
                EndDate = jdd ? Date.newInstance(2022, 12, 12) : Date.newInstance(2020, 12, 12)
            ) ;
            jdd = jdd ? false : true;  
            conts.add(con);    
        }
        if(doInsert) {
            insert conts;
        }

        return conts;
    }
}
