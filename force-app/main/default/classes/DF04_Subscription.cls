/***********************************************************************
	@Author: Jeremy Fardilha
	@name: DF04_Subscription
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Data factory for subscription
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
@isTest
public with sharing class DF04_Subscription {

    private static final String ELEMENTS_MANQUANTS  = AP01_ConstantUtility.ELEMENTS_MANQUANTS;
    private static final String MANUEL              = AP01_ConstantUtility.MANUEL;

    
    /*******************************************************************************
        *LBC Project                  	                - Créer des subs avec le lien account et Accès bénéficiaire
		@name   createSubs
        @param  Set<String>	compliences                 - Liste des compliances (normalement géré par des picklist)
        @param  ID	acc                                 - Id de l'account à lier
        @param  ID	benef                               - Id du beneficiaire à lier
        @param  SBQQ_Subscription__c subsParent         - A valoriser pour lier à un subs parent
        @param  Boolean	doInsert                        - Indique si on les insert ou pas
        @return List<SBQQ_Subscription__c>	            - List subs générée   
	*****************************************************************************/
    public static List<SBQQ_Subscription__c> createSubs(Set<String> compliences, ID acc, ID benef, SBQQ_Subscription__c subsParent,  Boolean doInsert) {
        List<SBQQ_Subscription__c> subs = new List<SBQQ_Subscription__c>();

            for (String  complience : compliences ) {
                SBQQ_Subscription__c immo = new SBQQ_Subscription__c(
                    SBQQ_Quantity__c  = 5, 
                    Status__c = ELEMENTS_MANQUANTS, 
                    ComplianceProcess__c = String.valueOf(complience), 
                    LeadPhone_c = '+33 6 00 00 00 66',
                    CompanyAddressCity__c = 'Paris',
                    CompanyAddressStreet__c = '11 rue de paris' ,      
                    CompanyAddressZipcode_c = '70001',
                    CompanyName_c = 'LBC',
                    CompanySiret_c ='01234567891122',
                    ContactFirstName_c = 'Jeremy',
                    ProductWorkflow_c = 'test',
                    PromoterDescription_c = 'Promote test',
                    ContactGender_c = 'Mr',
                    ContactLastName_c = 'FRD',
                    ContractEmail_c = 'test@gmail.com',
                    ProductWorkflow__c = 'yeah',
                    BeneficiaryAccess__c = benef, 
                    BeneficiaryAccount__c = acc,
                    Souscription_Conforme__c = false,
                    SBQQ_RequiredById__c = subsParent != null ? subsParent.ID : null,
                    SBQQ_EndDate__c = Date.newInstance(2022, 12, 12),
                    SBQQ_Contract__c = String.valueof((Math.random() * 10000)),
                    FlowType__c = MANUEL);
                    subs.add(immo);
            }
        
        if(doInsert) {
            insert subs;
        }

        return subs;
    }
}
