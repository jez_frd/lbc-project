/***********************************************************************
	@Author: Jeremy Fardilha
	@name: TH01_Subscription
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Trigger handler subscription
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class TH01_Subscription {
    
    /*******************************************************************************
        *LBC Project                  	                - Gestion de la création des subs
		@name   afterInsertHandler
        @param  list<SBQQ_Subscription__c>	subsNew     - Nouvelle valeur du trigger            
    *****************************************************************************/
    public static void afterInsertHandler(list<SBQQ_Subscription__c> subsNew){
        if(subsNew.size() > 0 && AP03_CheckRecursive.runOnce(0)) {
            SM02_Contract.updateContractEndDateFromSubs(subsNew);
        }
    }
    
    /*******************************************************************************
        *LBC Project                  	                - Gestion de l'update des subs
		@name   afterUpdateHandler
        @param  list<SBQQ_Subscription__c>	subsNew     - Nouvelle valeur du trigger    
        @param  list<SBQQ_Subscription__c>	subsOld     - Ancienne valeur du trigger 
    *****************************************************************************/
    public static void afterUpdateHandler(list<SBQQ_Subscription__c> subsNew, list<SBQQ_Subscription__c> subsOld ){
        if(subsNew.size() > 0 && AP03_CheckRecursive.runOnce(0)) {
            SM02_Contract.updateContractEndDateFromSubs(subsNew);
        }
    } 
}
