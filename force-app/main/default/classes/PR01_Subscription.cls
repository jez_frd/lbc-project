/***********************************************************************
	@Author: Jeremy Fardilha
	@name: PR01_Subscription
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Provider for Subscription
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class PR01_Subscription {

    /*******************************************************************************
        *LBC Project                  	                - Récupère les subscription selon le statut et la conformité désirés
		@name   getSubscriptionByStatutAndConformity
        @param  String	status                          - Status de la subscription
        @param  boolean	conformity                      - Conformité de la subscription
        @return String	      			                - Requête à exécuter      
	*****************************************************************************/
    static public String getSubscriptionByStatutAndConformity(String status, boolean conformity) {
        String request = 
            'SELECT Id, CompanyAddressCity__c, OrderNumber__c, CompanyAddressCountry__c, CompanyPhone__c, ContactGender__c, ContactLastName__c,'+ 
            'ContactFirstName__c, Companyindustry__c, CompanyName__c, PrintVolume__c, BroadcastSoftware__c, PromoterDescription__c, SBQQ_RequiredById__c,'+
            'FlowType__c, BroadcastPartner__c, PostalCode__c, Souscription_Conforme__c, ComplianceProcess__c, Status__c, AccessStoreState__c,'+ 
            'ProductWorkflow__c, AccessUserId__c, KilometerRadius__c, AccessStoreId__c, CompanyAddressStreet__c, CompanyAddressZipcode__c, LeadPhone__c,'+
            'ContractEmail__c, CompanySegment__c, CompanySiret__c '+
            'FROM SBQQ_Subscription__c ' +
            'WHERE Status__c = \'' + status + '\'' +
            ' AND Souscription_Conforme__c = ' + String.valueOf(conformity);
   
        return request;
    }

    /*******************************************************************************
        *LBC Project                  	                - Récupère les subscription les subs parents à mettre à jour
		@name   getSubscriptionByIdAndStatut
        @param  String	status                          - Status de la subscription
        @param  boolean	conformity                      - Conformité de la subscription
        @param  Set<Id>	parentId                        - Id des subs parents à mettre àjour
        @return List<SBQQ_Subscription__c> 	            - Liste des subs     
	*****************************************************************************/
    static public List<SBQQ_Subscription__c> getSubscriptionByIdAndStatut(String status, boolean conformity, Set<Id> parentId) {
		SObjectAccessDecision decision = Security.stripInaccessible(
			AccessType.READABLE,[
                SELECT 
                    Id, 
                    Souscription_Conforme__c 
			    FROM SBQQ_Subscription__c
				WHERE status__c = :status 
				AND Souscription_Conforme__c = :conformity 
                AND Id in :parentId
            ]
        );

        List<SBQQ_Subscription__c> subs = decision.getRecords();
        return subs;
    }

    /*******************************************************************************
        *LBC Project                  	                - Récupère les subs par id
		@name   getSubsById
        @param  Set<Id>	parentId                        - Id des subs à récupérer
        @return List<SBQQ_Subscription__c>	            - List des subs      
    *****************************************************************************/
    static public List<SBQQ_Subscription__c> getSubsById(Set<Id> idSubs) {
		SObjectAccessDecision decision = Security.stripInaccessible(
			AccessType.READABLE,[
                SELECT 
                    SBQQ_Contract__c,
                    TechAmendmentReason__c 
                FROM SBQQ_Subscription__c 
                WHERE ID in :idSubs
            ]);

            List<SBQQ_Subscription__c> subs = decision.getRecords();
            return subs;
    }
}
