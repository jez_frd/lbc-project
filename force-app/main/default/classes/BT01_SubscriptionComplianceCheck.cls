/***********************************************************************
	@Author: Jeremy Fardilha
	@name: BT01_SubscriptionComplianceCheck
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Batch class of BT01_SubscriptionComplianceCheck
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/

public class BT01_SubscriptionComplianceCheck implements Database.Batchable<sObject> {
    public static final String ELEMENTS_MANQUANTS = AP01_ConstantUtility.ELEMENTS_MANQUANTS;
    public static final String IMMO_NEUF          = AP01_ConstantUtility.IMMO_NEUF;
    public static final String CONFORME           = AP01_ConstantUtility.CONFORME;
    public static final String PARENT_BUNDLE      = AP01_ConstantUtility.PARENT_BUNDLE;
   
    /*******************************************************************************
        *LBC Project                  	          - Start of batch
		@name   start
        @param  String	bc				          - Batchable Context
        @return List<SBQQ_Subscription__c>        - List of  Subscription           
	*****************************************************************************/
    public Database.QueryLocator start(Database.BatchableContext bc) { 
        String query = PR01_Subscription.getSubscriptionByStatutAndConformity(ELEMENTS_MANQUANTS, false);
        if(Test.isRunningTest()){
            query += ' LIMIT 100';
        }
        //Logs.info('BT01_SubscriptionComplianceCheck','Query', query);
        return Database.getQueryLocator(query);
    }
    
    /*******************************************************************************
        *LBC Project                  	          - Execute of batch
		@name   execute
        @param  String	bc				          - Batchable Context
        @param  List<SBQQ_Subscription__c>	      - Result of the query of start        
	*****************************************************************************/
    public void execute(Database.BatchableContext bc, List<SBQQ_Subscription__c> scope) {
        try {
            //Vérification de la conformité des subs
            Map<String, List<SBQQ_Subscription__c>> subsListConformed = SM01_Subscription.checkConformity(scope);
            SObjectAccessDecision decision = Security.stripInaccessible(
                AccessType.UPDATABLE,
                subsListConformed.get(CONFORME));
            update decision.getRecords();  

            //Mise à jour des parents
            List<SBQQ_Subscription__c> subsListConformedParent = SM01_Subscription.setParentConforme(subsListConformed.get(PARENT_BUNDLE));
            SObjectAccessDecision decisionlist = Security.stripInaccessible(
                AccessType.UPDATABLE,
                subsListConformedParent);
            update decisionlist.getRecords();  
            Logs.info('BT01_SubscriptionComplianceCheck','All conformed subscriptions were updated', null);
        } catch(Exception e) {
            Logs.error('BT01_SubscriptionComplianceCheck','Error Batch Compliance Check', e);
        }
    }
    
    /*******************************************************************************
        *LBC Project                  	          - Finish of the batch
		@name   finish
        @param  String	bc				          - Batchable Context       
	*****************************************************************************/
    public void finish(Database.BatchableContext bc) {
        Logs.info('BT01_SubscriptionComplianceCheck', 'Finish', null);
    }
    
}