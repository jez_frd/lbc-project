/***********************************************************************
	@Author: Jeremy Fardilha
	@name: PR02_Contract
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Provider for CustomMetaData
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class PR03_CustomMetadata {

    /*******************************************************************************
        *LBC Project                  	                - Récupère les champs obligatoires pour immo neuf
		@name   getImmoNeufMdt
        @return List<String>	            			- List de field   
    *****************************************************************************/
    static public List<String> getImmoNeufMdt() {
		SObjectAccessDecision decision = Security.stripInaccessible(
			AccessType.READABLE,[
                SELECT 
                    MasterLabel
                FROM 
                    Immo_neuf_required_field__mdt
            ]);

            List<Immo_neuf_required_field__mdt> immoMdt = decision.getRecords();
            List<String> listImmoField = new List<String>();
            for(Immo_neuf_required_field__mdt immo : immoMdt){
                listImmoField.add(immo.MasterLabel);
            }


            return listImmoField;
    }
}
