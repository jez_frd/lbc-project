/***********************************************************************
	@Author: Jeremy Fardilha
	@name: Logs
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Logs class
	@Version: 1.0
	@ModifiedBy :Jeremy Fardilha
*************************************************************************/
public with sharing class AP02_Logs {

    public enum Level{
        INFO,
        TRACE,
        DEBUG,
        ERROR
     }


	/*******************************************************************************
        *LBC Project                  	          - Save information log
		@name   info
        @param  String	className				  - ClassName
        @param  String	task				      - Task name
        @param  String	description				  - Description of the error
                    
	*****************************************************************************/
    public static void info(String className, String task, String description){
        Logs__c log = new Logs__c();
        log.ClassName__c = className;
        log.Description__c=description;
        log.TaskName__c = task;
        log.log_level__c =Level.info.name();

        Insert log;
    }
    
    /*******************************************************************************
        *LBC Project                  	          - Save success log
		@name   trace
        @param  String	className				  - ClassName
        @param  String	task				      - Task name
        @param  String	description				  - Description of the error
                    
	*****************************************************************************/
    public static void trace(String className,String task,String description ){
        Logs__c log = new Logs__c();
        log.ClassName__c = className;
        log.TaskName__c = task;
        log.Description__c=description;
        log.log_level__c = Level.trace.name();
        Insert log;
    }
    
    /*******************************************************************************
        *LBC Project                  	          - Save exception log
		@name   info
        @param  String	className				  - ClassName
        @param  String	task				      - Task name
        @param  String	Exception				  - The exception        
	*****************************************************************************/
    public static void error(String className, String task, Exception e){
        Logs__c log = new Logs__c();
        log.TaskName__c = task;
        log.ClassName__c = className;
        log.Stacktrace__c = e.getStackTraceString();
        log.message_error__c = e.getMessage();
        log.log_level__c = Level.error.name();
        log.Line_Number__c = e.getLineNumber();
        Insert log;
    }
        
    /*******************************************************************************
        *LBC Project                  	          - Save HTTP request log
		@name   info
        @param  String	className				  - ClassName
        @param  String	methodName				  - Name of the method
        @param  String	description				  - Description of response
        @param  String	request				      - The request            
	*****************************************************************************/
    public static void logHTTPRequest(String className, String methodName, String description, HttpRequest request){
        Logs__c log = new Logs__c();
        log.ClassName__c = className;
    
        log.Description__c = description;
        log.log_level__c = Level.info.name();
        if(request != null){
            log.http_request__c = request.getBody();
                
        }
        Insert log;
    }

    /*******************************************************************************
        *LBC Project                  	          - Save HTTP request log
		@name   logHTTPResponse
        @param  String	className				  - ClassName
        @param  String	methodName				  - Name of the method
        @param  String	description				  - Description of response
        @param  String	response				  - the response        
	*****************************************************************************/
    public static void logHTTPResponse(String className, String methodName, String description, HttpResponse response){
        Logs__c log = new Logs__c();
        log.ClassName__c = className;
    
        log.Description__c = description;
        log.log_level__c = Level.info.name();
        if(response != null){
            log.http_response__c = response.getBody();
            log.http_status_code__c = response.getStatusCode();         
        }
        Insert log;
    }
        
}