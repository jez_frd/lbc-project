/***********************************************************************
	@Author: Jeremy Fardilha
	@name: DF02_AccesBeneficiaire
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Data factory for acces beneficiaire
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
@isTest
public with sharing class DF02_AccesBeneficiaire {
    
    /*******************************************************************************
        *LBC Project                  	                - Créer des benefs avec des accounts
		@name   createBeneficiaireWithAcc
        @param  List<Account>	numberOfAccounts        - Nombre d'account à lier
        @param  Boolean	doInsert                        - Indique si on les insert ou pas
        @return List<Acces_beneficiaire__c>	      	    - List d'accès bénéficiaire générée   
	*****************************************************************************/
    public static List<Acces_beneficiaire__c> createBeneficiaireWithAcc(List<Account> accs, Boolean doInsert) {
        
        List<Acces_beneficiaire__c> benefs = new List<Acces_beneficiaire__c>();

        for(Account acc : accs) {
            Acces_beneficiaire__c accBenef = new Acces_beneficiaire__c(
                Name = 'accBenef ' + acc.name + ' - ' + Math.random(),  
                AccessAccount__c = acc.ID,
                AccessEmail__c = acc.name  +'@test.com',
                AccessStoreState__c = 'OUAH',
                AccessUserId__c = 'OUAH',
                AccessCategory__c = 'RealEstate',
                Segmentation__c = 'SSII',
                AccessSalutation__c = 'Mme',
                AccessFirstname__c = 'Mimmi',
                AccessLastname__c = 'Montey',
                AccessAddressStreet__c = '11 nowhere',
                AccessAddressCountry__c = 'FR',
                AccessAddressCity__c = 'city',
                AccessAddressZipcode__c = '22000'
                );
                benefs.add(accBenef);
        }
      
        if(doInsert) {
            insert benefs;
        }

        return benefs;
    }

}