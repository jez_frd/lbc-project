/***********************************************************************
	@Author: Jeremy Fardilha
	@name: PR02_Contract
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Provider for Contract
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public with sharing class PR02_Contract {

    /*******************************************************************************
        *LBC Project                  	                - Récupère les contrats par id
		@name   getContractWithSubs
        @param  Set<Id>	idCons                          - Id des contrats à récupérer
        @return List<Contract>	            			- List de contrats    
    *****************************************************************************/
    static public List<Contract> getContractWithSubs(Set<Id> idCons) {
		SObjectAccessDecision decision = Security.stripInaccessible(
			AccessType.READABLE,[
				SELECT 
					Id, 
					EndDate, 
					(
						SELECT 
							Id, 
							SBQQ_EndDate__c, 
							SBQQ_TerminatedDate__c, 
							SBQQ_Contract__c 
							FROM SBQQ_Subscriptions__r
					)
				FROM Contract 
				WHERE Id IN :idCons
            ]);

            List<Contract> cons = decision.getRecords();
            return cons;
    }
}
