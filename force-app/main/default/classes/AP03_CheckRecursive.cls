/***********************************************************************
	@Author: Jeremy Fardilha
	@name: AP03_CheckRecursive
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Classe utility pour la récursivité
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
public class AP03_CheckRecursive{

    public static List<boolean> runOnce = new List<Boolean>{false,false,false,false};
    
    /*******************************************************************************
        *LBC Project                  	                - Permet de vérifier premier appel à un trigger (en cas de récursivité) :P
		@name   runOnce
        @param  integer	parentId                        - Nombre de récursivité voulu
        @return boolean	                                - Récursivité ou non      
    *****************************************************************************/
    public static boolean runOnce(integer index){
    
        if(Test.isRunningTest()) {
            return true;
        }
        
        if(!runOnce[index]){
             runOnce.set(index,true);
             System.debug('!! checkRecursive - Première entrée : ' + index);
             return true;
         } else {
             System.debug('!! checkRecursive - Pas première entrée : ' + index);
             return false;
         }
    }
}