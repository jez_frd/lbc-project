/***********************************************************************
	@Author: Jeremy Fardilha
	@name: AT01_SubscriptionTest
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Test class of AT01_Subscription
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
@isTest
private class AT01_SubscriptionTest {
        /*******************************************************************************
        *LBC Project                  	                - Test du trigger AT01
		@name   testSubsAfterUpdate
	    *****************************************************************************/
        @isTest static void testSubsAfterUpdate() {

        DF01_TestDataFactory.testDataFactory();
        List<SBQQ_Subscription__c> subs =  [SELECT Id FROM SBQQ_Subscription__c LIMIT 2 ];

        //couple 0 pour terminated null et endDate de la subs supérieur au end date du contrat
        //couple 1 pour le terminatedDate de la subs supérieur à celui du contrat
        subs[0].SBQQ_TerminatedDate__c = null;
        subs[0].SBQQ_EndDate__c =  Date.newInstance(2022, 12, 12);
        subs[1].SBQQ_TerminatedDate__c =  Date.newInstance(2022, 12, 12);
        subs[1].SBQQ_EndDate__c = Date.newInstance(2022, 12, 12);

        List<Contract> conts = [SELECT ID FROM Contract LIMIT 2];
        conts[0].SBQQ_Subscription__c =  subs[0].Id;
        conts[0].EndDate = Date.newInstance(2021, 12, 12);
        conts[1].SBQQ_Subscription__c =  subs[0].Id;
        conts[1].EndDate = Date.newInstance(2021, 12, 12);

        update conts;
        
        // Perform test
        Test.startTest();
            Database.SaveResult[]  result = Database.update(subs, false);
        Test.stopTest();

        for (Database.SaveResult sr : result) {
            System.assertEquals(true, sr.isSuccess());
        }

        conts = [SELECT ID FROM Contract LIMIT 2];
        Date dateToCheck = (Date.newInstance(2022, 12, 12));
        System.assertEquals(dateToCheck, conts[0].EndDate, 'Contrat terminated');
        System.assertEquals(dateToCheck, conts[1].EndDate, 'Contrat not terminated');
    }
}
