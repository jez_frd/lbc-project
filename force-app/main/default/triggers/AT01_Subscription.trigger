/***********************************************************************
	@Author: Jeremy Fardilha
	@name: AP01_Subscription
	@CreatedDate: 04/02/2021
	@Description :LBC Project - Apex trigger subscription
	@Version: 1.0
	@ModifiedBy :<Author Name>
*************************************************************************/
trigger AT01_Subscription on SBQQ_Subscription__c (after insert, after update) {
   
   
   
    if(Trigger.isBefore){
        // A ajouter plus tard     
    }
    
    if(Trigger.isAfter){
        
        if(Trigger.isInsert){
            TH01_Subscription.afterInsertHandler(Trigger.new);
        }
        
        // Call After Update methods.
        if(Trigger.isUpdate){
            TH01_Subscription.afterUpdateHandler(Trigger.new, Trigger.old);
        }
    }
}