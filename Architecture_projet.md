Architecture globale du projet
1) Les constantes ont été centralisées dans l’AP01_ConstantUtility. Et ont été redéclaré au haut de chaque classe en pointant l’AP01. 
   L’objectif est d’éviter les méthodes trop verbeuse par exemple AP0X_getX(AP01_ContanstUtility.BOUTIQUE, AP01_ConstantUtility.CONFORME…) qui aurait fait perdre en 
   lisibilité sur le code.
2) Les méthodes ont été dispatché dans des 
	a. SMXX (service manager) permettant d’obtenir seulement de la logique métier par objet.
	b. PRXX (provider) contenant les méthodes SOQL permettant la réutilisation dans d’autres classes par objet.
	c. BT pour les batchs.
	d. DF pour les DataFactory.
	e. AT pour les classes triggers.
	f. TH pour apex handler
	g. AP pour les autres classes apex.
3) Amélioration de la sécurité des requêtes SOQL.
4) Ajout de log
5) Les noms d'objets avec un namespace différent ont été modifiés pour être testable en sandbox. (Exemple SBQQ__Subscription__c -> SBQQ_Subscription__c)



SubscriptionComplianceCheck
1) Changement de la visibilité de global en public pour les batchs, recommandation SF
2) Déplacement de la requête dans une classe Provider afin d’être réutilisé ailleurs.
3) Déplacement du code execute dans un Service Manager pour plus de lisibilité et réutilisation.
4) Ajout d’un queryLocator pour by passed la limite DML.
5) Modification des asserts d’après les recommandations APEX PMD.

NB : Je pense qu’une amélioration était possible pour la PR01_Subcription afin de déclarer que une méthode et non deux.

Point 4 Explication technique
1) Pour la conformité « immo neuf », un custom meta data a été créé. Cela permet aux administrateurs de modifier facilement. 
   Si un field décrit dans cette CMT est vide dans l’objet « SBBQ__Subscription__c » , il sera ajouté dans « MissingInformations__c ».
2) Pour la conformité «pub », une formule boolean «isEffectiveDate »  a été créé avec la règle « TODAY() < EffectiveEndDate__c » 
    sur l’objet « SBBQ__Subscription__c ». Une vérification a été ajouté sur la classe BT01_SubscriptionComplianceCheck qui check si cette 
	formule est à « true ». Si c’est le cas, la subscription est conforme sinon elle est à « false » et l’information est ajouté à « MissingInformations__c ».

Account_DataFactory et AccesBeneficiaire_DataFactory
1) Une classe mère a été créée qui exploite les autres datafactory. Elle pourra être complétée afin de générer un JDD complet.

ContractEndDateAdapterTrigger
1) Ajout d’une classe de test
2) Vérification de la récursivité
3) Réorganisation du code en couche logique/métier/Soql.

